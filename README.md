# spring-springmvc-mybatis
##spring集成springmvc，mybatis
1. mybatis逆向工程配置
2. swagger2 api文档生成配置(http://localhost:8080/swagger-ui.html)
3. 日志配置
4. ehcache做mybatis二级缓存配置
5. 集成reids（spring-data-redis）
6. druid数据源的监控配置
