package com.tuxianchao.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.tuxianchao.ssm.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.TimeUnit;

/**
 * @author
 * @create 2018-05-06 14:11
 **/
@Controller
public class TestController {

    private Logger logger = LoggerFactory.getLogger(TestController.class);
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * @return
     */
    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/users", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Object userList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                           @RequestParam(value = "pageSize", required = false, defaultValue = "30") int pageSize) {
        Object result = null;
        logger.debug("请求开始....", "");
        if (redisTemplate.opsForValue().get("REDIS_KEY" + pageNum + pageSize) != null) {
            logger.info("redis 命中....");
            result = redisTemplate.opsForValue().get("REDIS_KEY" + pageNum + pageSize);
        } else {
            //写缓存 30s过期时间测试
            result = employeeService.getEmployee(pageNum, pageSize);
            redisTemplate.opsForValue().set("REDIS_KEY" + pageNum + pageSize, result, 60, TimeUnit.SECONDS);
        }
        logger.debug("请求结束....", "");
        return result;
    }
}
