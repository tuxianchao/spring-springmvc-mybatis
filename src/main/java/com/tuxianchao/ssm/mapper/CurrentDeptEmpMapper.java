package com.tuxianchao.ssm.mapper;

import com.tuxianchao.ssm.bean.CurrentDeptEmp;
import java.util.List;

public interface CurrentDeptEmpMapper {
    int insert(CurrentDeptEmp record);

    List<CurrentDeptEmp> selectAll();
}