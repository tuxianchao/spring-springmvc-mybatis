package com.tuxianchao.ssm.mapper;

import com.tuxianchao.ssm.bean.Departments;
import java.util.List;

public interface DepartmentsMapper {
    int deleteByPrimaryKey(String deptNo);

    int insert(Departments record);

    Departments selectByPrimaryKey(String deptNo);

    List<Departments> selectAll();

    int updateByPrimaryKey(Departments record);
}