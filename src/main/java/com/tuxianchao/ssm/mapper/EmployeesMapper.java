package com.tuxianchao.ssm.mapper;

import com.tuxianchao.ssm.bean.Employees;
import java.util.List;

public interface EmployeesMapper {
    int deleteByPrimaryKey(Integer empNo);

    int insert(Employees record);

    Employees selectByPrimaryKey(Integer empNo);

    List<Employees> selectAll();

    int updateByPrimaryKey(Employees record);
}