package com.tuxianchao.ssm.mapper;

import com.tuxianchao.ssm.bean.DeptEmpLatestDate;
import java.util.List;

public interface DeptEmpLatestDateMapper {
    int insert(DeptEmpLatestDate record);

    List<DeptEmpLatestDate> selectAll();
}