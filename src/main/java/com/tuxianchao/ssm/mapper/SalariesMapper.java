package com.tuxianchao.ssm.mapper;

import com.tuxianchao.ssm.bean.Salaries;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SalariesMapper {
    int deleteByPrimaryKey(@Param("empNo") Integer empNo, @Param("fromDate") Date fromDate);

    int insert(Salaries record);

    Salaries selectByPrimaryKey(@Param("empNo") Integer empNo, @Param("fromDate") Date fromDate);

    List<Salaries> selectAll();

    int updateByPrimaryKey(Salaries record);
}