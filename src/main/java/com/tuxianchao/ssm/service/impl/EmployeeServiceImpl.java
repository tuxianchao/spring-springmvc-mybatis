package com.tuxianchao.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tuxianchao.ssm.bean.Employees;
import com.tuxianchao.ssm.controller.TestController;
import com.tuxianchao.ssm.mapper.EmployeesMapper;
import com.tuxianchao.ssm.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author
 * @create 2018-05-06 14:58
 **/
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private EmployeesMapper employeesMapper;

    @Override
    public PageInfo getEmployee(int pageNum, int pageSize) {
        logger.debug("进入{},参数:{}....", new String[]{this.getClass().getSimpleName().toString(), pageNum + "\t" + pageSize});
        /**
         * 分页插件pagehelper
         */
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<Employees> pageInfo = new PageInfo<>(employeesMapper.selectAll());
        logger.debug("离开{}，返回值:{}....", new String[]{this.getClass().getSimpleName().toString(), pageInfo.toString()});
        return pageInfo;
    }
}
