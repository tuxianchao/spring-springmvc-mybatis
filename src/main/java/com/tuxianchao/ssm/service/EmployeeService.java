package com.tuxianchao.ssm.service;

import com.github.pagehelper.PageInfo;

/**
 * @author
 * @create 2018-05-06 14:56
 **/
public interface EmployeeService {

    PageInfo getEmployee(int pageNum, int pageSize);
}
